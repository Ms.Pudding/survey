package com.survey.web.service.survey;

import com.survey.web.mapper.survey.SurveyMapper;
import com.survey.web.vo.survey.SurveyVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
@AllArgsConstructor
public class SurveyService {
    //dependency injection mapper 생성자를 만들어준다 .allargscostructor가 없으면
    //mapper 생성자에 파라메터 값이 없어서 null이 리턴된다
        private final SurveyMapper mapper;

        public List<SurveyVO> getList() throws Exception {
            return mapper.getList();
        }

        public void register(SurveyVO surveyVO) throws Exception {
            mapper.create(surveyVO);
        }

}
