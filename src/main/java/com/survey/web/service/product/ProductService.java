package com.survey.web.service.product;

import com.survey.web.vo.product.ProductVO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ProductService {
    private static final Map<Long, ProductVO> store = new HashMap<>();
    private static long sequence = 0L; //long형의 0
    //상품 저장
    public ProductVO save(ProductVO productVO){
        productVO.setId(++sequence);
        store.put(productVO.getId(),productVO);
        return productVO;
    }
    //map에 저장해둔 상품 id조회
    public ProductVO getId(Long id){
        return store.get(id);
    }
    //store 맵에 있는 productVO들을 arrayList배열로 빼냄
    public List<ProductVO> getList(){
        return new ArrayList<>(store.values());
    }
    //상품 정보 수정할때 씀
    public void update(Long itemId,ProductVO updateParam){
        ProductVO findItem = getId(itemId);
        findItem.setProductName(updateParam.getProductName());
        findItem.setPrice(updateParam.getPrice());
        findItem.setQuantity(updateParam.getQuantity());
    }
    //이건 왜 하는걸까...?
    public void clearStore(){
        store.clear();
    }


}
