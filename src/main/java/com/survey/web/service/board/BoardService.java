package com.survey.web.service.board;

import com.survey.web.mapper.board.BoardMapper;
import com.survey.web.vo.board.BoardVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class BoardService {
    private final BoardMapper mapper;

    public List<BoardVO> getBoardList() throws Exception{
        return mapper.getBoardList();
    }

}
