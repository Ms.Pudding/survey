package com.survey.web.service.mail;

import com.survey.web.vo.mail.MailVO;
import lombok.AllArgsConstructor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import java.net.DatagramPacket;

@Service
@AllArgsConstructor
public class MailService {

    private JavaMailSender mailSender;

    private static final String MY_ADDRESS = "jieunjeong9494@gmail.com";

    public void mailSend(MailVO mailVO){
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(MailService.MY_ADDRESS); //정보를 받는 사람 (수신자 = 나)
        message.setFrom(mailVO.getAddress()); //form에 적은 이메일 ..
        message.setSubject(mailVO.getTitle());
        message.setText(mailVO.getMessage());

        mailSender.send(message);

    }
}
