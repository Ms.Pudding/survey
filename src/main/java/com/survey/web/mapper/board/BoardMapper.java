package com.survey.web.mapper.board;

import com.survey.web.service.board.BoardService;
import com.survey.web.vo.board.BoardVO;
import lombok.AllArgsConstructor;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BoardMapper {

    public List<BoardVO> getBoardList() throws Exception;

}
