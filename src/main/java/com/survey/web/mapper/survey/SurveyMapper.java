package com.survey.web.mapper.survey;

import com.survey.web.vo.survey.SurveyVO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
@Mapper
public interface SurveyMapper {

        //서베이 리스트 가져오기
        public List<SurveyVO> getList() throws Exception;

        public void create(SurveyVO surveyVO) throws Exception;

}
