package com.survey.web.controller.board;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class BoardController {

    @GetMapping("/board/list")
    public ModelAndView getBoardLit(){
        ModelAndView view = new ModelAndView();
        view.setViewName("board/boardList");
        return view;
    }

}
