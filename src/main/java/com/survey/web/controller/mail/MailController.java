package com.survey.web.controller.mail;

import com.survey.web.service.mail.MailService;
import com.survey.web.vo.mail.MailVO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
@AllArgsConstructor
public class MailController {
    private final MailService mailService;

    @GetMapping("/mail")
    public String disMail(){
        return "mail";
    }

    @PostMapping("/mail")
    public String execMail(MailVO mailVO){
      mailService.mailSend(mailVO);
        return "redirect:/mail/success";
    }

    @GetMapping("mail/success")
    public String successMail(){

        return "mailsent";
    }


}
