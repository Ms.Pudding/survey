package com.survey.web.controller.survey;

import com.survey.web.service.survey.SurveyService;
import com.survey.web.vo.survey.SurveyVO;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@AllArgsConstructor
public class SurveyApiController {
   private final SurveyService service;
    /*
     @allargsconstructor 가 없으면
     public SurveyApiController(Surveyservice service)
     이렇게 필드값에 각각 파라메터 값을 줘서 만들어야한다
     안만들면 service값이 null로 됨
      */
   @GetMapping("/api/survey/list")
    public SurveyVO getList(){
       try{
           List<SurveyVO> list = service.getList();
           return list.get(0);

       }catch(Exception e){
           e.printStackTrace();
       }
       return null;
   }
   @GetMapping("api/survey/content")
    public String getContent(@RequestParam Integer id){
       try{
           List<SurveyVO> list = service.getList();
           return list.get(id).getContent();
       }catch(Exception e){
           e.printStackTrace();
       }
       return null;
   }
   @GetMapping("api/survey/email/{id}")
    public String getEmail(@PathVariable Integer id){
       try{
           List<SurveyVO> list = service.getList();
           return list.get(id).getEmail();
       }catch(Exception e){
           e.printStackTrace();
       }
       return null;
   }
}
