package com.survey.web.controller.survey;

import com.survey.web.service.survey.SurveyService;
import com.survey.web.vo.survey.SurveyVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

//controller에서 요청을 받으면 , model을 통해 데이터를 가져오고 그 정보를 다시
// controller에 보내서 view로 보여줌
@Controller
public class SurveyController {

    @Autowired
    private SurveyService service;

    //서베이 list 보여줌
    @GetMapping("/list")
    //getList() 는 약간 인터네이스 같은 건데, 여기서 프로토타입이고
    //service에서 바디를 만들어 준 것이다.
    public ModelAndView getList(){
        ModelAndView view = new ModelAndView();
        //뷰의 경로, 어떤 페이지를 보내줄 것인지 알려준다.
        view.setViewName("views/survey");
        return view;
    }

    //submit 등록화면
    @PostMapping("/list")
    public ModelAndView register(Model model, SurveyVO surveyVO) throws Exception {
        service.register(surveyVO);
        model.addAttribute("msg","등록이 완료되었습니다.");
        ModelAndView view = new ModelAndView();
        view.setViewName("views/surveySuccess");
     return view;
    }


}
