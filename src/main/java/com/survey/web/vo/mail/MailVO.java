package com.survey.web.vo.mail;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor//써도 되고 안써도 되는 듯 ? mailvo(){} 이렇게 파라메터 값 없는 생성자 만듬
public class MailVO {
    private String address;
    private String title;
    private String message;
}
