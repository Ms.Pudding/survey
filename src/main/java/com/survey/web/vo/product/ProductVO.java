package com.survey.web.vo.product;

import lombok.Data;

//상품 도메인 객체 개발

@Data
public class ProductVO {
    private Long id; //상품 id
    private String productName; //상품명
    private Integer price; //상품 가격 null 가능성 0
    private Integer quantity; //상품 수량

    //디폴트 의존성 주입(생성자)
    public ProductVO(){
    }
    public ProductVO(String productName,Integer price,Integer quantity){
        this.productName = productName;
        this.price = price;
        this.quantity = quantity;
    }

}

