package com.survey.web.vo.survey;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SurveyVO {

        private String writerName;
        private String email;
        private int question1;
        private int question2;
        private String content;

}
