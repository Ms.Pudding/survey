package com.survey.web.vo.board;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class BoardVO {
    private int boardNo;
    private String title;
    private String writer;
    private String regDate;
}
